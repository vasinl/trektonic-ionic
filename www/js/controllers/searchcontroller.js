app.factory('FourSquareAPI', ['$http', function($http){
  var apiUrl = 'https://api.foursquare.com/v2/';
  var clientId        = '1G2PVP3034NCLB2EFDR23QOA220SYGN5UTC21MF1FVU1Z3AN';
  var clientSecret    = 'GXR4YTA031S4XXR20O3BAUMP2WSK34LHXZCVI4JYUS2U3ZX5';

  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth()+1; //January is 0!
  var yyyy = today.getFullYear();

  if(dd<10) {
      dd='0'+dd
  } 

  if(mm<10) {
      mm='0'+mm
  } 

  today = ''+yyyy+mm+dd;
    
  return {
    searchVenue: function(searchText, success) {
        $http({
            url: apiUrl + 'venues/search?',
            method: "GET",
            params: {
                client_id: clientId,
                client_secret: clientSecret,
                v: today,
                near: 'New York, NY',
                query: searchText,
                limit: 5
            }
        })
            .success(function(data) {
                success(data);
            });
    },
    getVenue: function(venueId, success) {
        $http({
            url: apiUrl + 'venues/' + venueId,
            method: "GET",
            params: {
                client_id: clientId,
                client_secret: clientSecret,
                v: today
            }
        })
            .success(function(data) {
                success(data);
            });
    }
  }
}]);

app.controller('SearchController', ['$scope', '$state', 'FourSquareAPI', function($scope, $state, FourSquareAPI) {
  console.log('SearchController');
 	
 	$scope.searchVenue = function(searchText){
	 	FourSquareAPI.searchVenue(searchText, function(data){
	 		$scope.venues = data.response.venues;
	        console.log(data);
	 	});
 	};

 	var venueId = "43695300f964a5208c291fe3";
 	$scope.getVenue = function(venueId){
	 	FourSquareAPI.getVenue(venueId, function(data){
	 		$scope.venueInfo = data.response.venue;
	 		console.log(data);
	 	});
 	}
 	$scope.resetInput = function() {
 		$scope.searchText = '';
 	}
}])

