app.controller('CityController', ['$scope', '$state', '$ionicSlideBoxDelegate', '$location', function($scope, $ionicLoading, $state, $ionicSlideBoxDelegate, $location) {
    // Called to navigate to the main app
	$scope.startApp = function() {
	$state.go('main');
	};
	$scope.next = function() {
	$ionicSlideBoxDelegate.next();
	};
	$scope.previous = function() {
	$ionicSlideBoxDelegate.previous();
	};

	// Called each time the slide changes
	$scope.slideHasChanged = function(index) {
	$scope.slideIndex = index;
	console.log('test', index);
	};
}])
