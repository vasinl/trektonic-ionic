app.controller('LMapController', ['$scope', '$ionicLoading','LMapFactory', '$ionicPopup', function($scope, $ionicLoading, LMapFactory, $ionicPopup) {
  angular.extend($scope, {
    initial: LMapFactory.london,
    markers: LMapFactory.markers,
    icons: LMapFactory.icons,
    // maxbounds: LMapFactory.regions.london,
    // legend: {
    //     position: 'bottomleft',
    //     colors: [ '#ff0000', '#28c9ff', '#0000ff', '#ecf386' ],
    //     labels: [ 'National Cycle Route', 'Regional Cycle Route', 'Local Cycle Network', 'Cycleway' ]
    // },
    defaults: {
    },
    tiles: LMapFactory.tiles.ocm,
    });

    $scope.setMap = function(style){
        console.log(LMapFactory.tiles);
        console.log(style);
        angular.extend($scope, {
            tiles: LMapFactory.tiles[style],
        });
        console.log($scope.tiles);
    }
    $scope.setZoom = function(zoomLevel) {
        $scope.london.zoom = zoomLevel;
        console.log($scope.london.zoom);
    }


    //Search Popup
    $scope.showSearch = function(){
       $scope.alert = $ionicPopup.show({
        templateUrl:  'templates/map.search.html',
        scope: $scope,
        title: 'Pick Your Location'
      });
    };

    $scope.sendOrder = function() {
      $scope.alert.close();
    };

    // Theme View
    $scope.themeChecked = false;
    $scope.toggleTheme = function() {
        if ($scope.themeChecked == false){
            return false;
        } 
        else {
            return true;
        }
    }

}])
