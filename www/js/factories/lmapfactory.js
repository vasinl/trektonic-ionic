app.factory('LMapFactory', function(){
    var mapStyle = {};    
    mapStyle.london = {
                    lat: 51.505,
                    lng: -0.09,
                    zoom: 9
                };
    mapStyle.icons = {
        default_icon: {},
        leaf_icon: {
            iconUrl: 'https://ss3.4sqi.net/img/categories_v2/arts_entertainment/movietheater_bg_32.png',
            shadowUrl: 'examples/img/leaf-shadow.png',
             iconSize:     [32, 32], // size of the icon
            shadowSize:   [50, 64], // size of the shadow
            iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
            shadowAnchor: [4, 62],  // the same for the shadow
            popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
        },
        div_icon: {
            type: 'div',
            iconSize: [230, 0],
            html: 'Using <strong>Bold text as an icon</strong>: Lisbon',
            popupAnchor:  [0, 0]
        },
        orange_leaf_icon: {
            iconUrl: 'examples/img/leaf-orange.png',
            shadowUrl: 'examples/img/leaf-shadow.png',
            iconSize:     [38, 95],
            shadowSize:   [50, 64],
            iconAnchor:   [22, 94],
            shadowAnchor: [4, 62]
        }
    };
    mapStyle.markers = {
            osloMarker: {
                lat: 51.505,
                lng: -0.09,
                // message: "I want to travel here!",
                focus: true,
                draggable: false,
                icon: mapStyle.icons.leaf_icon,
            },
            // osloMarker2: {
            //     lat: 51.505,
            //     lng: -0.08,
            //     message: "I want to travel here2222!",
            //     focus: true,
            //     draggable: false
            // },
        };
    //Bounds
    mapStyle.regions = {
        london: {
            northEast: {
                lat: 51.51280224425956,
                lng: -0.11681556701660155
            },
            southWest: {
                lat: 51.50211782162702,
                lng: -0.14428138732910156
            }
        }
    };
    mapStyle.legend = {
                    position: 'bottomleft',
                    colors: [ '#ff0000', '#28c9ff', '#0000ff', '#ecf386' ],
                    labels: [ 'National Cycle Route', 'Regional Cycle Route', 'Local Cycle Network', 'Cycleway' ]
                },
    mapStyle.defaults = {
                    tileLayer: "http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png"
                },
    mapStyle.tiles = {
        osm: {
            url: "http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
            options: {
                attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            },
        },
        ocm: {
            url: "http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png",
            options: {
               attribution: '&copy; <a href="http://www.opencyclemap.org/copyright">OpenCycleMap</a> contributors'
            },
        },
    };
    return mapStyle;
});
