// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('app', ['ionic','ngMap','ion-google-place','ngCordova', 'leaflet-directive'])

.run(['$ionicPlatform', function($ionicPlatform) {    
  // Parse.initialize("GBF51zk1dZnnzcXr3nzIQ2KIsjksPoMVoWYOWZRw", "8qCY3zjUBKto4VN4MOzQ92d73yxSxbU01aGHTdwG");
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      // StatusBar.styleDefault();
    }
  });
}])

.config(['$stateProvider','$urlRouterProvider',function($stateProvider,$urlRouterProvider) {  
  $stateProvider
    .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html',
      controller: 'LogInController'
    })
    .state('signup', {
      url: '/signup',
      templateUrl: 'templates/signup.html',
      controller: 'LogInController'
    })
    .state('forgotpassword', {
      url: '/forgot-password',
      templateUrl: 'templates/forgot-password.html'
    })
    .state('tabs', {
      url:'/tab',
      abstract: true,
      templateUrl: 'templates/tabs.html'
    })
    .state('tabs.dashboard', {
        url:'/dashboard',
          views: {
            'dashboard': {
              templateUrl: 'templates/dashboard.html'
            },
            'feature@tabs.dashboard' :{
              templateUrl: "templates/dashboard.feature.html"
            },
          }  
      })

    .state('tabs.city', {
          url:'/city',
            views: {
              city: {
                templateUrl: 'templates/city.html'
              }
            }  
        })
    .state('tabs.map', {
          url:'/map',
            views: {
              city: {
                templateUrl: 'templates/map.html',
                controller: 'LMapController'
              },
              'theme@tabs.map' :{
                templateUrl: "templates/map.theme.html",
              },
            }  
        })
    .state('tabs.setting', {
          url:'/setting',
            views: {
              setting: {
                templateUrl: 'templates/setting.html',
                controller: 'LocationController'
              }
            }  
        })
    .state('tabs.account', {
        url:'/account',
          views: {
            account: {
              templateUrl: 'templates/account.html',
              controller: 'AccountController'
            }
          }  
      })
  $urlRouterProvider.otherwise('/login');
}]);

// app.controller('MainController', ['$scope', function($scope) {
//   console.log('$scope');

// }])

